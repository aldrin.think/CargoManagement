package com.tf_staff.cargouser;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tf_staff.cargouser.Utill.AppConstants;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
//        , LocationListener
{

    SupportMapFragment mapFragment;
    LocationManager locationManager;
    PlaceAutocompleteFragment manualSearchAutocompleteFragment;
    EditText manualEditText = null;
    double fromLat;
    double fromLong;
    double toLat;
    double toLong;
    int mapFlag = 0;
//    int fromFlag = 0;
//    int toFlag = 0;
    MarkerOptions fromMarkerOptions;
    MarkerOptions toMarkerOptions;
    SharedPreferences sharedPreferences;
    Marker fromMarker;
    Marker toMarker;
    private GoogleMap mMap;

//    CargoInfo cargoInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        manualSearchAutocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        manualEditText = ((EditText) manualSearchAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input));
        manualEditText.setHint("Enter manually");

        manualEditText.setTextSize(25f);
        manualEditText.setTextColor(Color.BLACK);
        manualSearchAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                final LatLng latLng = place.getLatLng();
//                sourceLat = latLng.latitude;
//                sourceLong = latLng.longitude;
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                mMap.animateCamera(cameraUpdate);

            }

            @Override
            public void onError(Status status) {
                Log.e("error map: ", status.getStatusMessage());
            }
        });


//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MapsActivity.this);
        sharedPreferences = getSharedPreferences("com.tf_staff.cargouser.sharedPreferences", MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        SharedPreferences.Editor edit = sharedPreferences.edit();
//        editor.putString("","");
//        editor.apply();
//
//        sharedPreferences.getString("",)
    }

    private void confirmDialogForLocation() {
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
//                final Dialog dialog = new Dialog(getApplicationContext());
//                dialog.setContentView(R.layout.dialog_confirm_map_location);
//                dialog.setTitle("Confirm");
//                dialog.show();
//                Button okButton = (Button) dialog.findViewById(R.id.buttonOk);
//
//                Button cancelButton = (Button) dialog.findViewById(R.id.buttonCancel);
//
//                okButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        sourceLat = latLng.latitude;
//                        sourceLong = latLng.longitude;
//                        dialog.dismiss();
////                        getParkingSpaces(sourceLat, sourceLong);
//                    }
//                });
//                cancelButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });


                AlertDialog.Builder mapLocationConfirmBuilder = new AlertDialog.Builder(MapsActivity.this);
                final View locationConfirmView;
                locationConfirmView = getLayoutInflater().inflate(R.layout.dialog_confirm_map_location, null);
                Button fromLocationButton = locationConfirmView.findViewById(R.id.buttonFromLocation);

                Button toLocationButton = locationConfirmView.findViewById(R.id.buttonToLocation);

                mapLocationConfirmBuilder.setView(locationConfirmView);
                final AlertDialog mapLocationConfirmDialog = mapLocationConfirmBuilder.create();
                mapLocationConfirmDialog.show();

                fromLocationButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (fromMarker != null) {
                            fromMarker.remove();
                        }
                        fromLat = latLng.latitude;
                        fromLong = latLng.longitude;

                        fromMarkerOptions = addMarkerOptions(fromLat, fromLong);
                        fromMarker = mMap.addMarker(fromMarkerOptions);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString(AppConstants.FROM_LATT, String.valueOf(fromLat));
                        edit.putString(AppConstants.FROM_LONGI, String.valueOf(fromLong));
                        edit.apply();
                        mapFlag++;

                        mapLocationConfirmDialog.dismiss();
                    }
                });

                toLocationButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (toMarker != null) {
                            toMarker.remove();
                        }
                        toLat = latLng.latitude;
                        toLong = latLng.longitude;

                        toMarkerOptions = addMarkerOptions(toLat, toLong);
                        toMarker = mMap.addMarker(toMarkerOptions);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString(AppConstants.TO_LATT, String.valueOf(toLat));
                        edit.putString(AppConstants.TO_LONGI, String.valueOf(toLong));
                        edit.apply();
                        mapFlag++;

                        mapLocationConfirmDialog.dismiss();
                    }
                });



            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mapFlag >= 2) {
            super.onBackPressed();
//            SharedPreferences sharedPreferences =

        } else {
            Toast.makeText(MapsActivity.this, "plot both from and to locations", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mMap.setMyLocationEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
            } else {
                mMap.setMyLocationEnabled(true);
            }

        } else {
            mMap.setMyLocationEnabled(true);
        }


        // Add a marker in Delhi and move the camera
//        LatLng delhi = new LatLng(-34, 151);
//        LatLng myLocation = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLocation, 15);
//        mMap.animateCamera(cameraUpdate);
//        mMap.addMarkerOptions(new MarkerOptions().position(delhi).title("Marker in Delhi"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(delhi));
        confirmDialogForLocation();


    }

    private MarkerOptions addMarkerOptions(double lat, double longi) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(lat, longi))
                .title(lat + "," + longi)
//                .snippet(stringBuilder.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        return markerOptions;
    }

//    @Override
//    public void onLocationChanged(Location location) {
//
//    }
//
//    @Override
//    public void onStatusChanged(String s, int i, Bundle bundle) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String s) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String s) {
//
//    }
}
