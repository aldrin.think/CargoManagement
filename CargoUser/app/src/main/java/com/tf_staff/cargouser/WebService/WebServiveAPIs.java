package com.tf_staff.cargouser.WebService;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import com.google.gson.JsonObject;
import com.tf_staff.cargouser.Models.CargoInfo;
import com.tf_staff.cargouser.Models.UserInfo;
public interface WebServiveAPIs {

    @POST("RegisterServlet")
    public Call<JsonObject> registerUser(@Body UserInfo userInfo);

    @POST("LoginServlet")
    public Call<JsonObject> loginUser(@Body UserInfo userInfo);

    @POST("RequestServlet")
    public Call<JsonObject> requestCargo(@Body CargoInfo userInfo);


}
