package com.tf_staff.cargouser.Models;

public class UserInfo {
    private String name = "";
    private String username = "";
    private String gender = "";
    private String dob = "";
    private String email = "";
    private String address = "";
    private String number = "";
    private String password = "";
    private String state = "";
    private String locationStatusInMap = "";


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocationStatusInMap() {
        return locationStatusInMap;
    }

    public void setLocationStatusInMap(String locationStatusInMap) {
        this.locationStatusInMap = locationStatusInMap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
