package com.tf_staff.cargouser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.tf_staff.cargouser.Models.CargoInfo;
import com.tf_staff.cargouser.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargouser.Utill.AppConstants;
import com.tf_staff.cargouser.WebService.WebServiveAPIs;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CargoRequestActivity extends AppCompatActivity implements View.OnClickListener {

    EditText clientUserName = null;
    EditText itemName = null;
    EditText itemType = null;
    EditText itemWeight = null;
    EditText clientNumber = null;
    EditText recieverNumber = null;
    EditText fromLocationOfItem = null;
    EditText toLocationOfItem = null;

    TextView proceed = null;
    TextView mapTextView = null;

    CargoInfo cargoInfo = null;
    SharedPreferences locationStatusInMapSharedPreferences;

    String fromLat = "";
    String fromLongi = "";
    String toLat = "";
    String toLongi = "";
    String username = "";
    ProgressDialog pDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargo_request);

        clientUserName = (EditText) findViewById(R.id.editTextClientUsername);
        itemName = (EditText) findViewById(R.id.editTextItemName);
        itemType = (EditText) findViewById(R.id.editTextItemType);
        itemWeight = (EditText) findViewById(R.id.editTextItemWeight);
        clientNumber = (EditText) findViewById(R.id.editTextClientNumber);
        recieverNumber = (EditText) findViewById(R.id.editTextDestinationNumber);
        fromLocationOfItem = (EditText) findViewById(R.id.editTextFromAddress);
        toLocationOfItem = (EditText) findViewById(R.id.editTextToAddress);
//        fromMapButton = (Button) findViewById(R.id.From_location_map_button);
//        fromMapButton.setOnClickListener(this);
//        toMapButton = (Button) findViewById(R.id.To_location_map_button);
//        toMapButton.setOnClickListener(this);
        proceed = (TextView) findViewById(R.id.textViewProceed);
        proceed.setOnClickListener(this);
        mapTextView = (TextView) findViewById(R.id.textViewToPlotLocationOnMap);
        mapTextView.setOnClickListener(this);

        locationStatusInMapSharedPreferences = getSharedPreferences("com.tf_staff.cargouser.sharedPreferences", MODE_PRIVATE);
        username = locationStatusInMapSharedPreferences.getString(AppConstants.USERNAME, "default_value");
        if (!username.equals("default_value")) {
            clientUserName.setText(username);
        }
// else {
//            Toast.makeText(CargoRequestActivity.this, "Error", Toast.LENGTH_LONG).show();
//            finish();
//        }
    }

    public void showProgressDialog(){
        pDialog = new ProgressDialog(CargoRequestActivity.this);
        pDialog.setMessage("LOADING...");
        pDialog.show();
    }

    private void cargoRequestModel(){
        cargoInfo = new CargoInfo();
        cargoInfo.setUsername(username);
        cargoInfo.setName(itemName.getText().toString());
        cargoInfo.setType(itemType.getText().toString());
        cargoInfo.setWeight(itemWeight.getText().toString());
        cargoInfo.setClientNumber(clientNumber.getText().toString());
        cargoInfo.setReceiverNumber(recieverNumber.getText().toString());
        cargoInfo.setFromLocation(fromLocationOfItem.getText().toString());
        cargoInfo.setToLocation(toLocationOfItem.getText().toString());
//            cargoInfo.setFromLat(AppConstants.FROM_LATT);

        cargoInfo.setFromLat(fromLat);
        cargoInfo.setFromLongi(fromLongi);
        cargoInfo.setToLat(toLat);
        cargoInfo.setToLongi(toLongi);
    }

    private void cargoRequestWebservices(){
        showProgressDialog();
        Retrofit retrofit = new RetrofitExtension().getRetrofit();
        WebServiveAPIs webServiveAPIs = retrofit.create(WebServiveAPIs.class);
        webServiveAPIs.requestCargo(cargoInfo).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pDialog.dismiss();
                JsonObject body = response.body();
                String status = "";
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(body));
                    status = (String) jsonObject.get("loginstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                if (response.body().equals("success")){
                if (status.trim().equals("success")){
                    Toast.makeText(CargoRequestActivity.this,"Request Success",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CargoRequestActivity.this, MainActivity.class);
                    startActivity(intent);
                } else{
                    Toast.makeText(CargoRequestActivity.this,status,Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
                Toast.makeText(CargoRequestActivity.this,"Error",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == proceed.getId()) {
            fromLat = locationStatusInMapSharedPreferences.getString(AppConstants.FROM_LATT, "default_value");
            fromLongi = locationStatusInMapSharedPreferences.getString(AppConstants.FROM_LONGI, "default_value");
            toLat = locationStatusInMapSharedPreferences.getString(AppConstants.TO_LATT, "default_value");
            toLongi = locationStatusInMapSharedPreferences.getString(AppConstants.TO_LONGI, "default_value");
            cargoRequestModel();

            if(!cargoInfo.getFromLat().equals("default_value") ||
                    !cargoInfo.getFromLongi().equals("default_value") ||
                    !cargoInfo.getToLat().equals("default_value") ||
                    !cargoInfo.getToLongi().equals("default_value")){
                cargoRequestWebservices();
            } else{
                Toast.makeText(CargoRequestActivity.this,"plot locations on the map",Toast.LENGTH_SHORT).show();
                Toast.makeText(CargoRequestActivity.this,"to do so press 'plot to and from locations on the map ",Toast.LENGTH_SHORT).show();
            }

        } else if(view.getId() == mapTextView.getId()){
            Intent intent = new Intent(CargoRequestActivity.this,MapsActivity.class);
            startActivity(intent);

//            AlertDialog.Builder FromMap_builder = new AlertDialog.Builder(this);
////            FromMapView = getLayoutInflater().inflate(R.layout.dialog_map_4_from_address, null);
//            FromMapView = getLayoutInflater().inflate(R.layout.dialog_map, null);
//
////            newMapView = (MapView) FromMapView.findViewById(R.id.mapView);
//
////            mapFragment = (SupportMapFragment) getSupportFragmentManager()
////                    .findFragmentById(R.id.fromMap);
////            mapFragment.getMapAsync(this);
//
//
//            submitButton = (Button)FromMapView.findViewById(R.id.submitMapButton);
//
//            submitButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    FromMapView.invalidate();
//                }
//            });
//
//            FromMap_builder.setView(FromMapView);
//            AlertDialog FromMap_Dialog = FromMap_builder.create();
//            FromMap_Dialog.show();

        }
//        else if(view.getId() == R.id.To_location_map_button){
//
//        Intent intent = new Intent(CargoRequestActivity.this, MapsActivity.class);
//        startActivity(intent);
////            AlertDialog.Builder ToMap_builder = new AlertDialog.Builder(this);
////            ToMapView = getLayoutInflater().inflate(R.layout.dialog_map_4_to_address, null);
////
//////            newMapView = (MapView) FromMapView.findViewById(R.id.mapView);
////
////            mapFragment = (SupportMapFragment) getSupportFragmentManager()
////                    .findFragmentById(R.id.toMap);
////            mapFragment.getMapAsync(this);
////
////            submitButton = (Button)ToMapView.findViewById(R.id.submitMapButton);
////
////            submitButton.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
//////                    ToMapView.invalidate();
////                }
////            });
////
////            ToMap_builder.setView(ToMapView);
////            AlertDialog ToMap_Dialog = ToMap_builder.create();
////            ToMap_Dialog.show();
////
////
//        }
    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Delhi and move the camera
//        LatLng delhi = new LatLng(28, 77);
//        mMap.addMarker(new MarkerOptions().position(delhi).title("Marker in Delhi"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(delhi));
}
