package com.tf_staff.cargouser.Models;

import com.google.gson.annotations.SerializedName;

public class CargoInfo {

    private String type = "";
    private String id = "";
    private String weight = "";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username = "";
    @SerializedName("from_location")
    private String fromLocation = "";
    @SerializedName("to_location")
    private String toLocation = "";
    @SerializedName("from_lat")
    private String fromLat = "";
    @SerializedName("from_lng")
    private String fromLongi = "";

    @SerializedName("to_lat")
    private String toLat = "";

    @SerializedName("to_lng")
    private String toLongi = "";
    @SerializedName("contact_no")
    private String clientNumber = "";
    @SerializedName("d_contact_no")
    private String receiverNumber = "";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name = "";
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverNumber() {
        return receiverNumber;
    }

    public void setReceiverNumber(String receiverNumber) {
        this.receiverNumber = receiverNumber;
    }



    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getFromLat() {
        return fromLat;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromLongi() {
        return fromLongi;
    }

    public void setFromLongi(String fromLongi) {
        this.fromLongi = fromLongi;
    }

    public String getToLat() {
        return toLat;
    }

    public void setToLat(String toLat) {
        this.toLat = toLat;
    }

    public String getToLongi() {
        return toLongi;
    }

    public void setToLongi(String toLongi) {
        this.toLongi = toLongi;
    }
}
