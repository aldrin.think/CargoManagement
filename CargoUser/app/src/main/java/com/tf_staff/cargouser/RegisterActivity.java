package com.tf_staff.cargouser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.tf_staff.cargouser.Models.UserInfo;
import com.tf_staff.cargouser.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargouser.WebService.WebServiveAPIs;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText name = null;
    EditText username = null;
    EditText dob = null;
//    EditText gender = null;
    EditText state = null;
    EditText email = null;
    EditText address = null;
    EditText number = null;
    EditText password = null;
    EditText confirmPassword = null;
    TextView submit = null;
    String gender = "default_value";
    Spinner genderSpinner = null;
    ArrayAdapter<CharSequence> spinnerAdapter;

    UserInfo userInfo = null;
    ProgressDialog pDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        name = (EditText) findViewById(R.id.editTextName);
        username = (EditText) findViewById(R.id.editTextUsername);
        dob = (EditText) findViewById(R.id.editTextDOB);
//        gender = (EditText) findViewById(R.id.editTextGender);
        state = (EditText) findViewById(R.id.editTextState);
        email = (EditText) findViewById(R.id.editTextEmail);
        address = (EditText) findViewById(R.id.editTextAddress);
        number = (EditText) findViewById(R.id.editTextPhone);
        password = (EditText) findViewById(R.id.editTextPassword);
        confirmPassword = (EditText) findViewById(R.id.editTextConfirnPassword);

        genderSpinner = (Spinner) findViewById(R.id.spinnerGender);
        spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.gender, R.layout.support_simple_spinner_dropdown_item);
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        genderSpinner.setAdapter(spinnerAdapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!adapterView.getItemAtPosition(i).toString().equals("Select Gender")) {
                    gender = adapterView.getItemAtPosition(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
//                Toast.makeText(RegisterActivity.this,"select the gender",Toast.LENGTH_SHORT).show();
            }
        });


        submit = (TextView) findViewById(R.id.textViewSubmit);
        submit.setOnClickListener(this);

    }

    private void registrationModel() {
        if (password.getText().toString().equals(confirmPassword.getText().toString()) && !gender.equals("default_value")) {
            userInfo = new UserInfo();
            userInfo.setName(name.getText().toString());
            userInfo.setUsername(username.getText().toString());
            userInfo.setDob(dob.getText().toString());
//            userInfo.setGender(gender.getText().toString());

            userInfo.setGender(gender);

            userInfo.setEmail(email.getText().toString());
            userInfo.setAddress(address.getText().toString());
            userInfo.setState(state.getText().toString());
            userInfo.setNumber(number.getText().toString());
            userInfo.setPassword(password.getText().toString());
        } else {
            if((gender.equals("default_value"))||(gender.equals("Select Gender"))){
                Toast.makeText(RegisterActivity.this,"select the gender",Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(RegisterActivity.this, "passwords are not matching", Toast.LENGTH_SHORT).show();
                clearEditText(name);
                clearEditText(username);
                clearEditText(dob);
//                clearEditText(gender);
                clearEditText(address);
                clearEditText(state);
                clearEditText(number);
                clearEditText(email);
                clearEditText(password);
                clearEditText(confirmPassword);
            }
        }
    }


    public int validation() {
        if (name.getText().equals("") ||
                username.getText().equals("") ||
                dob.getText().equals("") ||
                address.getText().equals("") ||
                state.getText().equals("") ||
                number.getText().equals("")||
                email.getText().equals("") ||
                password.getText().equals("") ||
                confirmPassword.getText().equals("")){
//            String emailString = email.getText().
            if (
//                    !email.getText().toString().contains("email") &&
                    !email.getText().toString().substring(email.getText().toString().length()-5).equals(".com")
//                            &&
//                    !email.getText().toString().contains(".com")
                    ) {
                Toast.makeText(RegisterActivity.this, "not valid email ", Toast.LENGTH_SHORT).show();
                return 0;
            }
            return 0;
        } else {
            return 1;
        }
    }




    private void showProgerssDialog() {
        pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("LOADING...");
        pDialog.show();
    }

    private void registrationWebservice() {
        showProgerssDialog();
        Retrofit retrofit = new RetrofitExtension().getRetrofit();
        WebServiveAPIs webServiveAPIs = retrofit.create(WebServiveAPIs.class);
//        registrationModel();
            webServiveAPIs.registerUser(userInfo).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    pDialog.dismiss();
                    String status = "";
                    JsonObject body = response.body();
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(body));
                        status = (String) jsonObject.get("loginstatus");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (status.equals("true")) {
                        Toast.makeText(RegisterActivity.this, "Registration Success", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
//                    } else {
//                        Toast.makeText(RegisterActivity.this, "Invalid Username or Password", Toast.LENGTH_LONG).show();
////                    Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
////                    startActivity(intent);
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    pDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(RegisterActivity.this, "Connection Error", Toast.LENGTH_LONG).show();
                }
            });

    }

    public void clearEditText(EditText editText) {
        editText.setText("");

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == submit.getId()) {
            int flag = validation();
            if(flag == 1) {
                registrationModel();
                registrationWebservice();
            } else{
                Toast.makeText(RegisterActivity.this,"complete the request", Toast.LENGTH_LONG).show();
            }
        }

    }
}
