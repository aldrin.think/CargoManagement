package com.tf_staff.cargouser.Utill;

/**
 * Created by tf-staff on 14/9/17.
 */

public interface AppConstants {

    String FROM_LATT = "fromLattitude";
    String FROM_LONGI = "fromLongitude";
    String TO_LATT = "toLattitude";
    String TO_LONGI = "toLongitude";
    String USERNAME = "username";
}
