package com.tf_staff.cargouser;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.tf_staff.cargouser.Models.UserInfo;
import com.tf_staff.cargouser.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargouser.Utill.AppConstants;
import com.tf_staff.cargouser.WebService.WebServiveAPIs;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView requestCargoImageView = null;
    TextView requestCargoTextView = null;

    View loginView = null;
    //    Intent intent = null;
    EditText username = null;
    EditText password = null;
    Button loginButton = null;

    ActionBar actionBar = null;
    UserInfo userInfo = null;
    ProgressDialog pDialog = null;
    SharedPreferences sharedPreferences;

    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestCargoImageView = (ImageView) findViewById(R.id.RequestCargoImageView);
        requestCargoImageView.setOnClickListener(this);

        requestCargoTextView = (TextView) findViewById(R.id.RequestCargoTextView);
        requestCargoTextView.setOnClickListener(this);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(R.string.app_name);

        sharedPreferences = getSharedPreferences("com.tf_staff.cargouser.sharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(AppConstants.TO_LATT, "default_value");
        edit.putString(AppConstants.TO_LONGI, "default_value");
        edit.putString(AppConstants.FROM_LATT, "default_value");
        edit.putString(AppConstants.FROM_LONGI, "default_value");
        edit.apply();
//        SharedPreferences.Editor editor = sharedPreferences.edit();
    }

    public void clearEditText(EditText editText) {
        editText.setText("");

    }

    private void loginModel() {
        userInfo = new UserInfo();
        userInfo.setUsername(username.getText().toString());
        userInfo.setPassword(password.getText().toString());
    }

    private void showProgerssDialog() {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("LOADING...");
        pDialog.show();
    }

    private void loginWebService() {
        showProgerssDialog();
        Retrofit retrofit = new RetrofitExtension().getRetrofit();
        WebServiveAPIs webServiveAPIs = retrofit.create(WebServiveAPIs.class);
        webServiveAPIs.loginUser(userInfo).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                pDialog.dismiss();
                String status = "";
                JsonObject body = response.body();
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(body));
                    status = (String) jsonObject.get("loginstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status.equals("true")) {
                    Toast.makeText(MainActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString(AppConstants.USERNAME, username.getText().toString());
                    edit.apply();
                    dialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), CargoRequestActivity.class);
                    startActivity(intent);

//                    Intent intent = new Intent(login_builder.getContext(), CargoRequestActivity.class);

                } else {
//                    Toast.makeText(MainActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, "either username or password is wrong", Toast.LENGTH_SHORT).show();
                    clearEditText(username);
                    clearEditText(password);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                pDialog.dismiss();
                t.printStackTrace();
//                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.RequestCargoImageView || view.getId() == R.id.RequestCargoTextView) {
//            login_builder = new AlertDialog.Builder(MainActivity.this);
//            loginView = getLayoutInflater().inflate(R.layout.dialog_login, null);
//            username = (EditText) loginView.findViewById(R.id.username_editText_4login);
//            password = (EditText) loginView.findViewById(R.id.password_editText_4login);
//
//            loginButton = (Button) loginView.findViewById(R.id.submit_button_4login);
//
//            login_builder.setView(loginView);
//            loginDialog = login_builder.create();
//            loginDialog.show();
            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.dialog_login);

            username = (EditText) dialog.findViewById(R.id.username_editText_4login);
            password = (EditText) dialog.findViewById(R.id.password_editText_4login);

            loginButton = (Button) dialog.findViewById(R.id.submit_button_4login);

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (username.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "fill empty fields", Toast.LENGTH_SHORT).show();
                    } else {
//                            intent = new Intent(getApplicationContext(), CargoRequestActivity.class);
//                            startActivity(intent);
                        loginModel();
                        loginWebService();
//                        loginDialog.dismiss();
                    }


                }
            });

            dialog.show();
//            Intent intent = new Intent(MainActivity.this,CargoRequestActivity.class);
//            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cargo_message_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.register_button) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
