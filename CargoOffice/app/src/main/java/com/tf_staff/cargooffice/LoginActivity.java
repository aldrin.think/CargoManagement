package com.tf_staff.cargooffice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.tf_staff.cargooffice.Models.CargoData;
import com.tf_staff.cargooffice.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargooffice.WebserviceAPI.WebserviceAPIs;
import com.tf_staff.cargooffice.util.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    EditText usernameEditText = null;
    EditText passwordEditText = null;
    Button loginButton = null;

    CargoData cargoData = null;
    SharedPreferences sharedPreferences = null;

    ProgressDialog pDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEditText = (EditText) findViewById(R.id.editTextusernameLogin);
        passwordEditText = (EditText) findViewById(R.id.editTextPasswordLogin);

        loginButton = (Button) findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(LoginActivity.this);

        cargoData = new CargoData();
        sharedPreferences = getSharedPreferences("com.tf_staff.cargooffice.sharedPreferences",MODE_PRIVATE);
        if (!sharedPreferences.getString(AppUtils.USERNAME, "default_value").equals("default_value")){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void showProgressDialog(){
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("LOADING...");
        pDialog.show();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == loginButton.getId()){
            showProgressDialog();
            Retrofit retrofit = new RetrofitExtension().getRetrofit();
            WebserviceAPIs webservicesAPIs = retrofit.create(WebserviceAPIs.class);

            cargoData.setUsername(usernameEditText.getText().toString());
            cargoData.setPassword(passwordEditText.getText().toString());
            webservicesAPIs.login(cargoData).enqueue(new Callback<CargoData>() {
                @Override
                public void onResponse(Call<CargoData> call, Response<CargoData> response) {
                    pDialog.dismiss();
                    CargoData cargoData = response.body();
                    if(cargoData != null){
                        if (cargoData.getStatus().equals("true")) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(AppUtils.USERNAME, usernameEditText.getText().toString());
                            editor.apply();
                            Toast.makeText(LoginActivity.this, cargoData.getStatus(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }  else{
//                            Toast.makeText(LoginActivity.this, cargoData.getStatus()+"\n"+"invalid username or password",Toast.LENGTH_SHORT).show();
                            Toast.makeText(LoginActivity.this, "invalid username or password",Toast.LENGTH_LONG).show();
                            clearText(usernameEditText);
                            clearText(passwordEditText);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CargoData> call, Throwable t) {
                    pDialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(LoginActivity.this,"Connection Error",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    private void clearText(EditText editText){
        editText.setText("");
    }
}
