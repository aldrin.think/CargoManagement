package com.tf_staff.cargooffice.WebserviceAPI;

import com.tf_staff.cargooffice.Models.CargoData;
import com.tf_staff.cargooffice.Models.QRData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by tf-staff on 15/9/17.
 */

public interface WebserviceAPIs {

    @POST("WorkFinishedServlet")
    public Call<CargoData> qrScanData(@Body QRData qrData);
    @POST("LoginServlet")
    public Call<CargoData> login(@Body CargoData cargoData);

}