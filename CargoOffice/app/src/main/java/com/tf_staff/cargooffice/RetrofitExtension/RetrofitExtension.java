package com.tf_staff.cargooffice.RetrofitExtension;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitExtension {
    Retrofit retrofit = null;
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    public Retrofit getRetrofit(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.1.215:8085/CargoManagement/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
