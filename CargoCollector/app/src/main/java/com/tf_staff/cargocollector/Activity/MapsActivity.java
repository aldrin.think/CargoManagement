//package com.tf_staff.cargocollector.Activity;
//
//import android.app.ProgressDialog;
//import android.content.SharedPreferences;
//import android.support.v4.app.FragmentActivity;
//import android.os.Bundle;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.tf_staff.cargocollector.Models.QRData;
//import com.tf_staff.cargocollector.R;
//import com.tf_staff.cargocollector.RetrofitExtension.RetrofitExtension;
//import com.tf_staff.cargocollector.WebservicesAPIs.WebservicesAPIs;
//import com.tf_staff.cargocollector.util.AppUtils;
//
//import retrofit2.Retrofit;
//
//public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
//
//    private GoogleMap mMap;
//    SharedPreferences sharedPreferences = null;
//    String wId = "default_value";
//    String username = "default_value";
//
//    QRData qrData = null;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//        sharedPreferences = getSharedPreferences("com.tf_staff.cargocollector.sharedPreferences",MODE_PRIVATE);
//        wId = sharedPreferences.getString(AppUtils.WORK_ID, "default_value");
//        username = sharedPreferences.getString(AppUtils.USERNAME, "default_value");
//
//        qrData = new QRData();
//        collectorMapPostModel();
//
//    }
//
//
//    public void showProgressDialog() {
//        pDialog = new ProgressDialog(MessageActivity.this);
//        pDialog.setMessage("LOADING....");
//        pDialog.show();
//    }
//
//    private void collectorWebservices(){
//        Retrofit retrofit = new RetrofitExtension().getRetrofit();
//        WebservicesAPIs webservicesAPIs = retrofit.create(WebservicesAPIs.class);
//        WebservicesAPIs.
//    }
//
//    private void collectorMapPostModel(){
//
//    }
//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in delhi and move the camera
//        LatLng delhi = new LatLng(29, 77);
//        mMap.addMarker(new MarkerOptions().position(delhi).title("Marker in Delhi"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(delhi));
//    }
//}
