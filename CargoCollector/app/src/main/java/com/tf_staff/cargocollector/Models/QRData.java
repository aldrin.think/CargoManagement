package com.tf_staff.cargocollector.Models;

/**
 * Created by tf-staff on 15/9/17.
 */

public class QRData {

    private String qrcodeid = "";
    private String username = "";


    public String getQrcodeid() {
        return qrcodeid;
    }

    public void setQrcodeid(String qrcodeid) {
        this.qrcodeid = qrcodeid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
