package com.tf_staff.cargocollector.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tf_staff.cargocollector.Models.CargoData;
import com.tf_staff.cargocollector.R;
import com.tf_staff.cargocollector.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargocollector.WebservicesAPIs.WebservicesAPIs;
import com.tf_staff.cargocollector.util.AppUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.support.v7.appcompat.R.attr.title;

public class MessageActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LocationListener {
    ListView messageListView;
    String username = "";

    SharedPreferences sharedPreferences;

    CargoData cargoData = null;

    ProgressDialog pDialog = null;

    List<CargoData> cargoInfo  = null;

    CargoMessagesAdapter listViewAdapterForMessageActivity = null;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        messageListView = (ListView) findViewById(R.id.message_listView);
        sharedPreferences = getSharedPreferences("com.tf_staff.cargocollector.sharedPreferences",MODE_PRIVATE);
        username = sharedPreferences.getString(AppUtils.USERNAME, "default_value");
        getSupportActionBar().setTitle(username);

        messageWebservice();
        messageListView.setOnItemClickListener(this);
    }


    public void showProgressDialog() {
        pDialog = new ProgressDialog(MessageActivity.this);
        pDialog.setMessage("LOADING....");
        pDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        TextView textView = view.findViewById(R.id.textViewWorkId);
//        String[] wId = textView.getText().toString().split(":");
////        Toast.makeText(this, textView.getText().toString(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, wId[1], Toast.LENGTH_SHORT).show();
//
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString(AppUtils.WORK_ID, wId[1]);
//        editor.apply();
//        Intent intent = new Intent(MessageActivity.this,MapsActivity.class);
////        intent.putExtra()
//        startActivity(intent);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Uri uri = Uri.parse("http://maps.google.com/maps?"
                    + "saddr=" + location.getLatitude() + "," + location.getLongitude() + "&daddr=" + cargoData.getLat() + "," + cargoData.getLng());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
            locationManager.removeUpdates(MessageActivity.this);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public class CargoMessagesAdapter extends BaseAdapter {

        List<CargoData> cargoDatas = null;
        public CargoMessagesAdapter(List<CargoData> cargoDatas ) {
            this.cargoDatas = cargoDatas;
        }
        @Override
        public int getCount() {
            return cargoDatas.size();
        }

        @Override
        public Object getItem(int i) {
            return cargoDatas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder = null;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.message_list, null);
                viewHolder = new ViewHolder();

                viewHolder.workIdTextView = (TextView) view.findViewById(R.id.textViewWorkId);
                viewHolder.requestIdTextView = (TextView) view.findViewById(R.id.textViewRequestId);
                viewHolder.fromAddressTextView = (TextView) view.findViewById(R.id.textViewFromAddress);
                viewHolder.contactNumberTextView = (TextView) view.findViewById(R.id.textViewContactNumber);
                viewHolder.pathTextView = view.findViewById(R.id.textViewPath);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
//            CargoData cargoData = (CargoData) getItem(i);
            cargoData = (CargoData) getItem(i);
            viewHolder.workIdTextView.setText("Work Id:" + cargoData.getWorkId());
            viewHolder.requestIdTextView.setText("Request Id: " + cargoData.getRequsetId());
            viewHolder.fromAddressTextView.setText(cargoData.getFromAddress());
            viewHolder.contactNumberTextView.setText("Contact: " + cargoData.getContactNumber());
            viewHolder.pathTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    String provider = LocationManager.NETWORK_PROVIDER;
                    String[] permissions = new String[] {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(permissions, 1);
                        } else {
                            locationManager.requestLocationUpdates(provider, 0, 0, MessageActivity.this);
                        }
                    } else {
                        locationManager.requestLocationUpdates(provider, 0, 0, MessageActivity.this);
                    }
                }
            });
            return view;
        }

        private class ViewHolder {
            TextView pathTextView;
            TextView workIdTextView;
            TextView requestIdTextView;
            TextView fromAddressTextView;
            TextView contactNumberTextView;
        }


    }

    private CargoData messagePostToModel(){
        CargoData usernameValidation = new CargoData();
        if (username.equals("default_value")){
            Toast.makeText(MessageActivity.this,"Invalid User",Toast.LENGTH_SHORT).show();
//            finish();
        } else{
            usernameValidation.setUsername(username);
        }
        return usernameValidation;
    }

    private void messageWebservice(){
        showProgressDialog();
        Retrofit retrofit = new RetrofitExtension().getRetrofit();
        WebservicesAPIs webServiveAPIs = retrofit.create(WebservicesAPIs.class);
        
        webServiveAPIs.workData(messagePostToModel()).enqueue(new Callback<List<CargoData>>() {
            @Override
            public void onResponse(Call<List<CargoData>> call, Response<List<CargoData>> response) {
                pDialog.dismiss();
//                List<CargoData> cargoInfo = response.body();
                cargoInfo = response.body();
                if (cargoInfo != null) {
//                    CargoMessagesAdapter listViewAdapterForMessageActivity = new CargoMessagesAdapter(cargoInfo);
                    listViewAdapterForMessageActivity = new CargoMessagesAdapter(cargoInfo);
                    messageListView.setAdapter(listViewAdapterForMessageActivity);
                }else{
                    Toast.makeText(MessageActivity.this,"No messages for you",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<CargoData>> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                Toast.makeText(MessageActivity.this,"Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_message, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemLogout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(AppUtils.USERNAME, "default_value");
            editor.apply();
            Intent intent = new Intent(MessageActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}

