package com.tf_staff.cargocollector.WebservicesAPIs;

import com.tf_staff.cargocollector.Models.CargoData;
import com.tf_staff.cargocollector.Models.QRData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by tf-staff on 15/9/17.
 */

public interface WebservicesAPIs {

    @POST ("WorkFinishedServlet")
    public Call<CargoData > qrScanData(@Body QRData qrData);

    @POST("WorkServlet")
    public Call<List<CargoData>> workData(@Body CargoData cargoData);


    @POST("LoginServlet")
    public Call<CargoData> login(@Body CargoData cargoData);


    @POST("WorkDetailsServlet")
        public Call<CargoData> getLocation(@Body CargoData cargoData);
}
