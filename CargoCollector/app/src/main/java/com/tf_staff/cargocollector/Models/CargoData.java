package com.tf_staff.cargocollector.Models;

import com.google.gson.annotations.SerializedName;

public class CargoData {

    private String lat;
    private String lng;
    @SerializedName("from_location")
    private String fromAddress = "";
    @SerializedName("to_location")
    private String toAddress = "";
    private String username = "";
    private String password = "";
    @SerializedName("work_id")
    private String workId = "";
    @SerializedName("request_id")
    private String requsetId = "";
    private String status = "";
    @SerializedName("contact_no")
    private String contactNumber = "";

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRequsetId() {
        return requsetId;
    }

    public void setRequsetId(String requsetId) {
        this.requsetId = requsetId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }


    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

}
