package com.tf_staff.cargocollector.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.tf_staff.cargocollector.Models.CargoData;
import com.tf_staff.cargocollector.Models.QRData;
import com.tf_staff.cargocollector.R;
import com.tf_staff.cargocollector.RetrofitExtension.RetrofitExtension;
import com.tf_staff.cargocollector.WebservicesAPIs.WebservicesAPIs;
import com.tf_staff.cargocollector.util.AppUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

//  -->     qr scanner data

    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;

    TextView qrStatus = null;
    //    TextView fromTextView = null;
//    TextView toTextView = null;
    TextView submitTextView = null;

    SurfaceView qrReader = null;
    BarcodeDetector barcodeDetector = null;
    CameraSource cameraSource = null;
    SurfaceHolder surfaceHolder = null;
//  <--     qr scanner data

    QRData qrData = null;
    String qrValue = "default_value";
    String username = "default_value";
    //    String [] qrSplitVlaues = new String[2];
    SharedPreferences sharedPreferences = null;

    ProgressDialog pDialog = null;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        fromTextView = (TextView) findViewById(R.id.CargoFrom_value_textView);
//        toTextView = (TextView) findViewById(R.id.CargoTo_value_textView);
        submitTextView = (TextView) findViewById(R.id.Submit_textView);
        submitTextView.setOnClickListener(this);

        qrStatus = (TextView) findViewById(R.id.qr_textView);

        qrData = new QRData();

        sharedPreferences = getSharedPreferences("com.tf_staff.cargocollector.sharedPreferences", MODE_PRIVATE);
        String actionBarTitleString = sharedPreferences.getString(AppUtils.USERNAME, "default_user");
        getSupportActionBar().setTitle(actionBarTitleString);

//  -->     qr scanner data

        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, PERMISSION_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        qrReader = (SurfaceView) findViewById(R.id.qrReader);
        qrReader.setZOrderMediaOverlay(true);
        surfaceHolder = qrReader.getHolder();
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        if (!barcodeDetector.isOperational()) {
            Toast.makeText(getApplicationContext(), "Sorry, coudn't setup the detecter", Toast.LENGTH_SHORT).show();
            this.finish();
        }



        qrReader.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                cameraSource = new CameraSource.Builder(MainActivity.this, barcodeDetector)
                        .setFacing(cameraSource.CAMERA_FACING_BACK)
                        .setRequestedFps(24)
                        .setAutoFocusEnabled(true)
                        .setRequestedPreviewSize(1280, 720)
                        .build();
                try {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(qrReader.getHolder());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            }
        });
//        Detector detector = new Detector();
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections detections) {
                final SparseArray<Barcode> barcodeSparseArray = detections.getDetectedItems();
                if (barcodeSparseArray.size() > 0) {
                    final Barcode barcode = (Barcode) barcodeSparseArray.valueAt(0);


                    qrValue = (String) barcode.displayValue;
                    Log.e("qrValueDecoded", qrValue);
                    if (!qrValue.equals("")) {
                        Vibrator v = (Vibrator) getSystemService(MainActivity.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        v.vibrate(500);
                    }

//                      qrModel();
//                    qrWebservice();

//                    qrStatus.post(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            qrStatus.setText(barcode.displayValue);
//                        }
//                    });
                }
            }
        });

//  <--     qr scanner data

    }

    private void releaseCameraAndPreview() {
        surfaceHolder = null;
        if (cameraSource != null) {
            cameraSource.release();
            cameraSource = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setFacing(cameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(24)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1280, 720)
                .build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.cameraSource.stop();
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("LOADING....");
        pDialog.show();
    }

    private void qrModel() {
//        qrSplitVlaues = qrValue.split(":");

//        qrData.setQrcodeid(qrSplitVlaues[0]);
//        qrData.setUsername(qrSplitVlaues[1]);
        if (!qrValue.equals("")) {
            Toast.makeText(MainActivity.this, "QR code fetched" + qrValue, Toast.LENGTH_LONG).show();
        }

        username = sharedPreferences.getString(AppUtils.USERNAME, "default_value");
        if (!username.equals("default_value")) {
            qrData.setUsername(username);
            qrData.setQrcodeid(qrValue);
        } else {
            Toast.makeText(MainActivity.this, "default_username detected", Toast.LENGTH_SHORT).show();
        }
    }

    private void qrWebservice() {
        showProgressDialog();
        Retrofit retrofit = new RetrofitExtension().getRetrofit();
        WebservicesAPIs webservicesAPIs = retrofit.create(WebservicesAPIs.class);

        webservicesAPIs.qrScanData(qrData).enqueue(new Callback<CargoData>() {
            @Override
            public void onResponse(Call<CargoData> call, Response<CargoData> response) {
                pDialog.dismiss();
                CargoData cargoData = response.body();
                if (cargoData.getStatus().equals("success")) {
                    Toast.makeText(MainActivity.this, "valid cargo", Toast.LENGTH_SHORT).show();
                    showProgressDialog();
                    qrStatus.setText(cargoData.getStatus());
//                    fromTextView.setText(cargoData.getFromAddress());
//                    toTextView.setText(cargoData.getToAddress());
                } else {
                    Toast.makeText(MainActivity.this, cargoData.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CargoData> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.Submit_textView) {
            qrModel();
            qrWebservice();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.itemMessage) {
            Intent intent = new Intent(MainActivity.this, MessageActivity.class);
            startActivity(intent);
        }
        if (item.getItemId() == R.id.itemLogout) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(AppUtils.USERNAME, "default_value");
            editor.apply();
            cameraSource.stop();
            finish();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
